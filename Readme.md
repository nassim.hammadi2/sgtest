######Installation du kata via docker#######
Se rendre dans le dossier SGTest puis executer dans le terminal les commandes suivantes: 

docker build -t nassimhammadi/sgtest .

Puis, en s'assurant qu'aucun autre programme n'utilise le port 8080 sur votre poste: 
docker run -p 8080:8080 -d nassimhammadi/sgtest


######Exectution du kata######
Via le logiciel postman, configurez une nouvelle requette post aux URL suivante, en respectant le schéma de données associé : 

1- 
	http://localhost:8080/deposit 
	Schéma de données : 
	{
		"id" : "1",
		"amount" : 356
	}

2- 
	http://localhost:8080/withdrawal
	Schéma de données : 
	{
		"id" : "1",
		"amount" : 223
	}

3-
	http://localhost:8080/history
	Schéma de données : 
	{
		"id" : "1"
	}


######Explications######
J'ai façoné le KATA de façon à respecter au mieux le pattern Event Sourcing CQRS afin de garder une trace de l'ensemble des évements ayant eu lieu sur le compte "sgRecruiter". Etant donné la contrainte technique impliquant la non persistance des données, j'ai simulé un eventStore à l'aide d'un array.

Durée de développement : 2h



Ceci est un projet Node.js, si les modules nodes sont déjà installés sur votre environnement, la simple commande "node server.js" suffira pour executer le kata.