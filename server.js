var express = require('express');

function bankAccount(id, owner, balance, lastEventTimeStamp){
	this.id = id;
	this.owner = owner;
	this.balance = balance;
	this.lastEventTimeStamp = lastEventTimeStamp;
}

function depositPerformed(accountId, amount, type, date){
	this.accountId = accountId;
	this.amount = amount;
	this.type = type;
	this.date = date;
}

function withDrawalPerformed(accountId, amount, type, date){
	this.accountId = accountId;
	this.amount = amount;
	this.type = type;
	this.date = date;
}

function accountCreated(accountId, type, date){
	this.accountId = accountId;
	this.type = type;
	this.date = date;
}

function currentDate(){
	let ts = Date.now();
	let date_ob = new Date(ts);
	let date = date_ob.getDate();
	let month = date_ob.getMonth() + 1;
	let year = date_ob.getFullYear();
	let hours = date_ob.getHours();
	let minutes = date_ob.getMinutes();
	let seconds = date_ob.getSeconds();
	return (date+'/'+month+'/'+year+'-'+hours+':'+minutes+':'+seconds);
}

//Simulate persistence of EventStore (Event Sourcing) and Database
var arrayAsEventStore = [];
var arrayAsDb = [];

//Init 

var sgRecruiter = new bankAccount('1','sgRecruiter',0, currentDate());
var accountCreated = new accountCreated('1','accountCreated', currentDate());
arrayAsDb.push(sgRecruiter);
arrayAsEventStore.push(accountCreated);


//Html server
var server = express();
server.use(express.json());
server.listen(8080);
 
//Deposit
server.post('/deposit', function(request, response) {
  var json = JSON.parse(JSON.stringify(request.body));
  if(json.id == '1'){
  	var deposit = new depositPerformed(json.id,json.amount,'depositPerformed', currentDate());
  	arrayAsEventStore.push(deposit);
  	var lastBkAccount = arrayAsDb[arrayAsDb.length - 1];
  	var bkAccount = new bankAccount(lastBkAccount.id,lastBkAccount.owner,lastBkAccount.balance,currentDate());
  	bkAccount.balance = lastBkAccount.balance + deposit.amount;
  	arrayAsDb.push(bkAccount);
  	response.send(request.body);
  }
  else{
  	response.send(404);
  }
  
});

//Withdrawals
server.post('/withdrawal', function(request, response) {
  var json = JSON.parse(JSON.stringify(request.body));
  if(json.id == '1'){
  	var lastBkAccount = arrayAsDb[arrayAsDb.length - 1];
  	if(json.amount > lastBkAccount.balance){
  		response.send(404);
  	}
  	else{
  		var withdrawal = new withDrawalPerformed(json.id,json.amount,'withDrawalPerformed', currentDate());
  		arrayAsEventStore.push(withdrawal);
  		var bkAccount = new bankAccount(lastBkAccount.id,lastBkAccount.owner,lastBkAccount.balance,currentDate());
  		bkAccount.balance = lastBkAccount.balance - withdrawal.amount;
  		arrayAsDb.push(bkAccount);
  		response.send(request.body);
  	}
  }
  else{
  	response.send(404);
  }
});


//History
server.post('/history', function(request, response) {
  var json = JSON.parse(JSON.stringify(request.body));
  if(json.id == '1'){
  	response.json({ 
    	Events: arrayAsEventStore, 
    	Balances: arrayAsDb, 
    
  	});
  }
  else{
  	response.send(404);
  }
  
});